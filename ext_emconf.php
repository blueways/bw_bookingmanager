<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Booking Manager',
    'description' => 'A generic bookingmanager',
    'category' => 'plugin',
    'author' => 'Maik Schneider',
    'author_email' => 'm.schneider@blueways.de',
    'state' => 'stable',
    'version' => '11.0.4',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
