# Booking Manager v11

## Install

* via composer
* include TypoScript setup and constants
* include route enhancer in site config:

```
imports:
  - resource: 'EXT:bw_bookingmanager/Configuration/Routing/Api.yaml'
  - resource: 'EXT:bw_bookingmanager/Configuration/Routing/Ics.yaml'
```
